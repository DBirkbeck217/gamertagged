import Home from "./Home";
import Profile from "./Profile";
import NotFound from "./NotFound";
import MessageFeed from "./MessageFeed.js";
import Register from "./Register";
import Questionnaire from "./Questionnaire"

export default {
  Home: { path: "/", component: Home },
  Profile: { path: "/profile/:username", component: Profile },
  MessageFeed: { path: "/messagefeed/:username", component: MessageFeed },
  Register: { path: "/register", component: Register },
  Questionnaire: { path: "/questionnaire", component: Questionnaire },
  NotFound: { path: "*", component: NotFound }
};
