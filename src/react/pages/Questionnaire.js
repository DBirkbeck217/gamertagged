import React from "react";
import "antd/dist/antd.css";
import "./Questionnaire.css";
import { Carousel, Radio, Card } from "antd";

import { userIsAuthenticated } from "../HOCs";
// function onChange(e) {
//   console.log(`checked = ${e.target.checked}`);
// }
class Questionnaire extends React.Component {
  state = {
    dotPosition: "top"
  };

  handlePositionChange = ({ target: { value: dotPosition } }) =>
    this.setState({ dotPosition });
  render() {
    const { dotPosition } = this.state;
    return (
      <>
        <div style={{ height: "100vh" }}>
          <Radio.Group
            onChange={this.handlePositionChange}
            value={dotPosition}
            style={{ marginBottom: 8 }}
          ></Radio.Group>
          <Carousel dotPosition={dotPosition}>
            <div>
              <Card title="Welcome Darlyzabeth!">
                <p>Let's answer some questions!</p>
              </Card>
            </div>
            <div>
              <Card title="1">
                <p>1</p>
              </Card>
            </div>
            <div>
              <Card title="2">
                <p>2</p>
              </Card>
            </div>
            <div>
              <Card title="3">
                <p>3</p>
              </Card>
            </div>
          </Carousel>
        </div>
      </>
    );
  }
}

export default userIsAuthenticated(Questionnaire);
