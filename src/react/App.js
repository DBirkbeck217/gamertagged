import React from "react";
import { Switch, Route } from "./components";
import { NavLink } from "react-router-dom";
import pages from "./pages";
import { Menu, Icon, Layout } from "antd";
import { withAsyncAction, connect } from "./HOCs";
import "./App.css";
const { Header, Footer, Sider, Content } = Layout;

class App extends React.Component {
  render() {
    return (
      <div className="App">
        <Header style={{ backgroundColor: "rgb(54, 76, 121)" }}>
          <h1>GamerTagged</h1>
        </Header>
        <Layout>
          <Sider width={256} style={{ backgroundColor: "rgb(54, 76, 121)" }}>
            <Menu
              theme="dark"
              onClick={this.handleClick}
              style={{ width: 256 }}
              defaultSelectedKeys={["1"]}
              defaultOpenKeys={["sub1"]}
              mode="inline"
            >
              <Menu.Item key="2">
                <NavLink to={`/questionnaire/`} activeClassName="chosen">
                  <Icon type="home" />
                  Home
                </NavLink>
              </Menu.Item>
              <Menu.Item key="1">
                <NavLink
                  to={`/profile/${this.props.username}`}
                  activeClassName="chosen"
                >
                  <Icon type="user" />
                  Profile
                </NavLink>
              </Menu.Item>

              <Menu.Item>
                <NavLink to="/" onClick={this.handleLogout}>
                  <Icon type="logout" />
                  Logout
                </NavLink>
              </Menu.Item>
            </Menu>
          </Sider>
          <Layout>
            <Content>
              <Switch>
                {Object.entries(pages).map(([routeName, routeObj]) => (
                  <Route
                    key={routeName}
                    exact
                    path={routeObj.path}
                    component={routeObj.component}
                  />
                ))}
              </Switch>
            </Content>
            <Footer style={{ backgroundColor: "rgb(54, 76, 121)" }}></Footer>
          </Layout>
        </Layout>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    username: state.auth.login.result && state.auth.login.result.username
  };
};

export default connect(mapStateToProps)(withAsyncAction("auth", "logout")(App));
