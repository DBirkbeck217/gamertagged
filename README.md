## Gamertagged

Gamertagged is a platform for finding friends to game with based around personality traits. It uses a process similar to major dating applications to match you up with like minded individuals.
